import tkinter as tk
from GeneralGameBoard import GeneralGameBoard
from SimpleGameBoard import SimpleGameBoard
# create base window
base_window = tk.Tk()
base_window.title("Main Window")
base_window.geometry("800x600")

def redraw_board():
    widget = base_window.grid_slaves(row=1, column=0)[0]
    widget.grid_forget()
    widget.destroy()
    if(game_type.get() == "general"):
        gameboard = GeneralGameBoard(int(board_size_field.get())).grid(row=1, column=0, rowspan=10, columnspan= 10)
    else:
        gameboard = SimpleGameBoard(int(board_size_field.get())).grid(row=1, column=0, rowspan=10, columnspan= 10)


SOSText = tk.Label(base_window, text="SOS", font=(25)).grid(row=0, column=0, padx=(50,50), pady=10) #create text

game_type = tk.StringVar()
game_type.set("simple")
simple_game_button = tk.Radiobutton(base_window, text = "Simple game", variable = game_type, value= "simple").grid(row=0, column=1, padx=(10,0), pady=10)
general_game_button = tk.Radiobutton(base_window, text = "General game", variable = game_type, value= "general").grid(row=0, column=2, padx=10, pady=10)


board_size_button = tk.Button(base_window, text="Make board of size:", command=redraw_board).grid(row=0, column=3, padx=(20,10), pady=10)

defaultValue = tk.StringVar(base_window, value='10')
board_size_field = tk.Entry(base_window, textvariable=defaultValue, width = 3)
board_size_field.grid(row= 0, column=4, pady=10 )


gameboard = SimpleGameBoard(int(board_size_field.get())).grid(row=1, column=0, rowspan=10, columnspan= 10)


# Start the GUI event loop
base_window.mainloop()