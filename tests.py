import unittest
import tkinter as tk
from GameBoard import GameBoard
from SimpleGameBoard import SimpleGameBoard
from GeneralGameBoard import GeneralGameBoard

class tests(unittest.TestCase):
    def test_verify_turn_swap(self):
        gameboard = GameBoard(3)
        self.assertEqual(gameboard.whose_turn.get(), "blue")  # initial starting turn is blue
        gameboard.on_button_click(2,2) #after clicking on the board,
        self.assertEqual(gameboard.whose_turn.get(), "red")  # it should be red's turn

    def test_verify_board_size(self):
        gameboard = GameBoard(3)
        self.assertEqual(gameboard.size, 3)

    def test_verify_S_move(self):
        gameboard = GameBoard(3)
        gameboard.blue_choice.set("S")
        gameboard.on_button_click(2,2)
        self.assertEqual(gameboard.buttons[2][2].cget('text'), "S")

    def test_verify_O_move(self):
        gameboard = GameBoard(3)
        gameboard.blue_choice.set("O")
        gameboard.on_button_click(2,2)
        self.assertEqual(gameboard.buttons[2][2].cget('text'), "O")

    def test_simple_game_board_blue_win(self):
        #after an sos has been made by blue
        gameboard = SimpleGameBoard(3)
        gameboard.red_choice.set("O")
        gameboard.on_button_click(0,0)
        gameboard.on_button_click(0,1)
        gameboard.on_button_click(0,2)

        #the game should have ended
        self.assertEqual(gameboard.ended, True)

        #the winning text should be properly displayed
        self.assertEqual(gameboard.current_turn_text.get(), "Winner is: ")

        #and finally, the winner should be blue
        self.assertEqual(gameboard.whose_turn.get(), "blue")

    def test_simple_game_board_red_win(self):
        #after an sos has been made by red
        gameboard = SimpleGameBoard(3)
        gameboard.blue_choice.set("O")
        gameboard.on_button_click(2,2)
        gameboard.on_button_click(0,0)
        gameboard.on_button_click(0,1)
        gameboard.on_button_click(0,2)

        #the game should have ended
        self.assertEqual(gameboard.ended, True)

        #the winning text should be properly displayed
        self.assertEqual(gameboard.current_turn_text.get(), "Winner is: ")

        #and finally, the winner should be red
        self.assertEqual(gameboard.whose_turn.get(), "red")

    def test_simple_game_board_draw(self):
        #after the board has been filled up
        gameboard = SimpleGameBoard(1)
        gameboard.on_button_click(0,0)

        #the game should have ended
        self.assertEqual(gameboard.ended, True)

        #the winning text should be properly displayed
        self.assertEqual(gameboard.current_turn_text.get(), "Winner is: ")

        #and finally, it should be a draw
        self.assertEqual(gameboard.whose_turn.get(), "a draw!")


    def test_general_game_board_blue_win(self):
        #board fills up, where blue has made 4 SOSs
        gameboard = GeneralGameBoard(3)
        gameboard.on_button_click(0,0)
        gameboard.red_choice.set("O")
        gameboard.on_button_click(1,1)
        gameboard.red_choice.set("S")
        gameboard.on_button_click(2,2)
        gameboard.on_button_click(0,1)
        gameboard.on_button_click(2, 1)
        gameboard.on_button_click(0, 2)
        gameboard.on_button_click(2, 0)
        gameboard.on_button_click(1, 0)
        gameboard.on_button_click(1, 2)


        #the winning text should be properly displayed
        self.assertEqual(gameboard.current_turn_text.get(), "Winner is: ")

        #the winner should be blue
        self.assertEqual(gameboard.whose_turn.get(), "blue")

        #blue should have a score of 4
        self.assertEqual(gameboard.blue_score.get(), 4)

        #red should have a score of 0
        self.assertEqual(gameboard.red_score.get(), 0)

    def test_general_game_board_draw(self):

        #after the board is filled up
        gameboard = GeneralGameBoard(20)
        for i in range(20):
            for j in range(20):
                gameboard.on_button_click(i,j)

        #the winning text should be properly displayed
        self.assertEqual(gameboard.current_turn_text.get(), "Winner is: ")

        #the winner should be blue
        self.assertEqual(gameboard.whose_turn.get(), "a draw!")

        #blue should have a score of 0
        self.assertEqual(gameboard.blue_score.get(), 0)

        #red should have a score of 0
        self.assertEqual(gameboard.red_score.get(), 0)

    def test_red_computer_move(self):
        gameboard = SimpleGameBoard(3)
        #after red has been chosen to be a computer
        gameboard.red_player.set("Computer")
        #and after blue takes their turn
        gameboard.on_button_click(0,0)
        #the computer should have made a move, and it should be blue's turn again
        self.assertEqual(gameboard.whose_turn.get(),"blue")

    def test_computer_simple_game(self):
        gameboard = SimpleGameBoard(10)
        #after players have been chosen to computer
        gameboard.blue_player.set("Computer")
        gameboard.red_player.set("Computer")
        #intial click
        gameboard.on_button_click(0,0)

        #the rest of the game should be automated! meaning only one click to start, and the game should now be over
        self.assertEqual(gameboard.ended, True)

if __name__ == '__main__':
    unittest.main()