import tkinter as tk
from GameBoard import GameBoard

class GeneralGameBoard(GameBoard):
    def __init__(self, size = 3):
        super().__init__(size)

    def on_button_click(self, row, col):
        super().on_button_click(row,col)
        for row in range(self.size):
            for col in range(self.size):
                if (self.buttons[row][col].cget('text') == ''):
                    return
        if (self.red_score.get() == self.blue_score.get()):
            super().tie()
        elif (self.red_score.get() > self.blue_score.get()):
            super().red_won()
        else:
            super().blue_won()