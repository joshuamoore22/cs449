import tkinter as tk
import random
import time

class GameBoard(tk.Frame):
    def __init__(self, size=3):
        super().__init__()
        self.record_file = open("lastgame.txt", "w")
        self.size = size
        self.buttons = [[None for _ in range(size)] for _ in range(size)] #declare 2d array
        self.create_buttons('#f0f0f0')
        self.whose_turn = tk.StringVar()
        self.whose_turn.set("blue")
        self.blue_score = tk.IntVar()
        self.blue_score.set(0)
        self.red_score = tk.IntVar()
        self.red_score.set(0)

        #turn stuff
        self.current_turn_text = tk.StringVar()
        self.current_turn_text.set("Current turn:")
        tk.Label(self, textvariable=self.current_turn_text, font=(25)).grid(row=size+1, column=0, padx=10, pady=10,columnspan = 8)  # create text
        tk.Label(self, textvariable=self.whose_turn, font=(25)).grid(row=size + 1, column=8, padx=10, pady=10, columnspan = 10)

        #blue player stuff
        self.blue_player = tk.StringVar()
        self.blue_player.set("Player")
        tk.Label(self, text="Blue").grid(row=0, column=0, padx=10, pady=10, rowspan=2)
        tk.Radiobutton(self, text="Player", variable=self.blue_player, value="Player" ).grid(row=1, column=0, padx=10, pady=10, rowspan=2)
        tk.Radiobutton(self, text="Computer", variable=self.blue_player, value="Computer" ).grid(row=2, column=0, padx=10, pady=10, rowspan=2)
        tk.Label(self, textvariable=self.blue_score).grid(row=3, column=0, padx=10, pady=10, rowspan=2)

        self.blue_choice = tk.StringVar()
        self.blue_choice.set("S")
        blue_s = tk.Radiobutton(self, text="S", variable=self.blue_choice, value="S" ).grid(row=5, column=0,padx=(10, 0),pady=10, rowspan=2)
        blue_o = tk.Radiobutton(self, text="O", variable=self.blue_choice, value="O" ).grid(row=7, column=0,padx=(10, 0),pady=10, rowspan=2)

        #red player stuff
        tk.Label(self, text="Red").grid(row=0, column=size+1, padx=10, pady=10, rowspan=2)
        self.red_player = tk.StringVar()
        self.red_player.set("Player")
        tk.Radiobutton(self, text="Player", variable=self.red_player, value="Player" ).grid(row=1, column=size+1, padx=10, pady=10, rowspan=2)
        tk.Radiobutton(self, text="Computer", variable=self.red_player, value="Computer" ).grid(row=2, column=size+1, padx=10, pady=10, rowspan=2)
        tk.Label(self, textvariable=self.red_score).grid(row=3, column=size+1, padx=10, pady=10, rowspan=2)
        self.red_choice = tk.StringVar()
        self.red_choice.set("S")
        red_s = tk.Radiobutton(self, text="S", variable=self.red_choice, value="S" ).grid(row=5, column=size+1,padx=(10, 0),pady=10, rowspan=2)
        red_o = tk.Radiobutton(self, text="O", variable=self.red_choice, value="O" ).grid(row=7, column=size+1,padx=(10, 0),pady=10, rowspan=2)

    def update_player_score(self):
        if (self.whose_turn.get() == "red"):
            self.red_score.set(self.red_score.get() + 1)
        elif (self.whose_turn.get() == "blue"):
            self.blue_score.set(self.blue_score.get() + 1)

    def create_buttons(self, color):
        for row in range(self.size):
            for col in range(self.size):
                button = tk.Button(self, bg = color, text='', height=1, width=3, borderwidth= .5, command=lambda r=row, c=col: self.on_button_click(r, c))
                button.grid(row=row, column=col + 1)
                self.buttons[row][col] = button

    def blue_won(self):
        self.current_turn_text.set("Winner is: ")
        self.whose_turn.set("blue")
        self.record_file.close()

    def red_won(self):
        self.current_turn_text.set("Winner is: ")
        self.whose_turn.set("red")
        self.record_file.close()

    def tie(self):
        self.current_turn_text.set("Winner is: ")
        self.whose_turn.set("a draw!")
        self.record_file.close()

    def draw_sos(self, row1, col1, row2, col2, row3, col3):
        self.update_player_score()
        if (self.buttons[row1][col1].cget('bg') == '#f0f0f0' or self.buttons[row1][col1].cget('bg') == self.whose_turn.get()):
            self.buttons[row1][col1].config(bg=self.whose_turn.get(), fg = "snow")
        else:
            self.buttons[row1][col1].config(bg="dark violet", fg = "snow")

        if (self.buttons[row2][col2].cget('bg') == self.whose_turn.get() or self.buttons[row2][col2].cget('bg') == '#f0f0f0'):
            self.buttons[row2][col2].config(bg=self.whose_turn.get(), fg="snow")
        else:
            self.buttons[row2][col2].config(bg="dark violet", fg = "snow")

        if (self.buttons[row3][col3].cget('bg') == self.whose_turn.get() or self.buttons[row3][col3].cget('bg') == '#f0f0f0'):
            self.buttons[row3][col3].config(bg=self.whose_turn.get(), fg="snow")

    def check_for_sos_given_s(self,row, col):
        #default bounds, assuming out of bounds
        row_lower = row
        col_lower = col
        row_upper = row + 1
        col_upper = col + 1

        #if not out of bounds, then allow us to check over there
        if (row > 1):
            row_lower = row - 1
        if (row < self.size - 2):
            row_upper = row + 2
        if (col > 1):
            col_lower = col - 1
        if (col < self.size - 2):
             col_upper = col + 2

        for i_row in range(row_lower, row_upper):
            for i_col in range(col_lower, col_upper):
                if self.buttons[i_row][i_col].cget('text') == 'O':
                    if self.buttons[2 * i_row - row][2 * i_col - col].cget('text') == 'S':
                        self.draw_sos(row, col, i_row, i_col, 2 * i_row - row, 2 * i_col - col)

    def check_for_sos_given_o(self, row, col):
        # corner and edge cases
        # (like, literal edge cases (the SOS is on the edge (i like parenthesis (and i think im funny))))
        if(row == 0 or row == self.size - 1):
            if (col == 0 or col == self.size - 1):
                return #corner O can never be an SOS
            if (self.buttons[row][col - 1].cget('text') == 'S' and self.buttons[row][col + 1].cget('text') == 'S'):
                self.draw_sos(row, col - 1, row, col, row, col + 1)
                return
            return
        if(col == 0 or col == self.size - 1):
            if (row == 0 or row == self.size - 1):
                return #corner O can never be an SOS
            if (self.buttons[row - 1][col].cget('text') == 'S' and self.buttons[row + 1][col].cget('text') == 'S'):
                self.draw_sos(row - 1, col, row, col, row + 1, col)
                return
            return

        # check for 4 possible sos
        if(self.buttons[row-1][col-1].cget('text') == 'S' and self.buttons[row+1][col+1].cget('text') == 'S'):
            self.draw_sos(row - 1, col - 1, row, col, row + 1, col + 1)
        if (self.buttons[row - 1][col].cget('text') == 'S' and self.buttons[row + 1][col].cget('text') == 'S'):
            self.draw_sos(row - 1, col, row, col, row + 1, col)
        if (self.buttons[row - 1][col + 1].cget('text') == 'S' and self.buttons[row + 1][col - 1].cget('text') == 'S'):
            self.draw_sos(row - 1, col + 1, row, col, row + 1, col - 1)
        if (self.buttons[row ][col - 1].cget('text') == 'S' and self.buttons[row][col + 1].cget('text') == 'S'):
            self.draw_sos(row, col - 1, row, col, row, col + 1)

    def robot_move(self):
        self.on_button_click(random.randint(0, self.size - 1), random.randint(0, self.size - 1))

    def robot_turn(self):
        current_turn = self.whose_turn.get()
        if(current_turn == "red"):
            if(random.randint(0,1) == 0):
                self.red_choice.set("S")
            else:
                self.red_choice.set("O")
        else:
            if(random.randint(0,1) == 0):
                self.blue_choice.set("S")
            else:
                self.blue_choice.set("O")
        while(current_turn == self.whose_turn.get() and not self.current_turn_text.get() == "Winner is: "):
            self.robot_move()

    def record_move(self, row, col):
        if (self.whose_turn.get() == "red"):
            self.record_file.write("red made an " + self.red_choice.get() + " at " + row.__str__() + ", " + col.__str__() +"\n" )
        else:
            self.record_file.write("blue made an " + self.blue_choice.get() + " at " + row.__str__() + ", " + col.__str__() + "\n")
    def on_button_click(self, row, col):
        if (self.buttons[row][col].cget('text') != ''): #if the spot is taken, don't do anything
            return
        self.record_move(row, col)
        if (self.whose_turn.get() == "red"):
            self.buttons[row][col].config(text=self.red_choice.get())
            if(self.red_choice.get() == 'S'):
                self.check_for_sos_given_s(row, col)
            elif(self.red_choice.get() == 'O'):
                self.check_for_sos_given_o(row, col)
            self.whose_turn.set("blue")
            if (self.blue_player.get() == "Computer"):
                self.robot_turn()
        else:
            self.buttons[row][col].config(text=self.blue_choice.get())
            if(self.blue_choice.get() == 'S'):
                self.check_for_sos_given_s(row, col)
            elif(self.blue_choice.get() == 'O'):
                self.check_for_sos_given_o(row, col)
            self.whose_turn.set("red")
            if (self.red_player.get() == "Computer"):
                self.robot_turn()