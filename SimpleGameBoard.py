import tkinter as tk
import random
from GameBoard import GameBoard

class SimpleGameBoard(GameBoard):
    def __init__(self, size = 3):
        super().__init__(size)
        self.ended = False

    def on_button_click(self, row, col):
        if self.ended:
            return
        super().on_button_click(row,col)
        if (self.red_score.get() > 0):
            super().red_won()
            self.ended = True
        elif (self.blue_score.get() > 0):
            super().blue_won()
            self.ended = True
        for row in range(self.size):
            for col in range(self.size):
                if (self.buttons[row][col].cget('text') == ''):
                    return
        super().tie()
        self.ended = True